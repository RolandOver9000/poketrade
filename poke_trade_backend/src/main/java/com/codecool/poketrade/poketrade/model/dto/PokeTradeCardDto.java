package com.codecool.poketrade.poketrade.model.dto;

import com.codecool.poketrade.poketrade.model.PokemonType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PokeTradeCardDto {

    private Long id;
    private String name;
    private int price;
    private PokemonType typeOne;
    private PokemonType typeTwo;

}
