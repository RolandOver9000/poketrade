package com.codecool.poketrade.poketrade.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class PokeTradeCard {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(mappedBy = "pokeTradeCard")
    private Moveset moveset;

    private int price;
    private PokemonType typeOne;
    private PokemonType typeTwo;

    @Column(nullable = false)
    private LocalDateTime creationDate;

}
