package com.codecool.poketrade.poketrade.model.dto;

import lombok.*;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PokeTradeUserDto {

    private Long id;
    private String username;
    private String email;
    private LocalDateTime registrationDate;
    private String password;


}
