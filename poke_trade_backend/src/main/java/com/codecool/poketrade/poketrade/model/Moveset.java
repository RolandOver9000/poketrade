package com.codecool.poketrade.poketrade.model;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Moveset {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne
    @MapsId
    private PokeTradeCard pokeTradeCard;

    private String move1;
    private String move2;
    private String move3;
    private String move4;

}
