# PokeTrade #

### What is this repository for? ###

This is a web application for trading Pokemons. The idea based on a fan game where people are selling Pokemons in chat.
It is hard to track who is selling and what. The goal is make an application where people can sell/buy Pokemons.

### Current features ###
* You can list your Pokemon to sell. Its picture will be fetched from a third party API called PokeApi.
* Authentication, authorization
* You can filter the Pokemons.
* Profile page with some data and with your own Pokemons.
* Pokemon cards background coloring is dynamic, based on its type.

### Planned features ###
* Ability to sell/buy Pokemons.
* Implement card packs, so the user can get random cards instead of filling out a form.
* Additional data for Pokemons. (Game specific data)
* Modify the user's cards in filtering so he/she can see what are his/her cards.

### Used Techonoliges ###
* Spring
* Spring Boot
* React
* PostgreSQL